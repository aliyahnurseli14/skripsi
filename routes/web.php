<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('index');
});
use App\Http\Controllers\uploadController;
Route::resource('upload', App\Http\Controllers\uploadController::class);
Route::get('sdt', [uploadController::class, 'indexSdt']);