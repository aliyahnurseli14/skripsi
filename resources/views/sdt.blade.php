<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
  </head>
  <body>
    @php
    $counter = 0;
    $hasil = array();
    $sqc=array();
    $dpc=array();
    @endphp
    <div class="container">
        <div class="card mt-5 px-5 py-5">
        <form action="{{route('upload.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
            <input type="file" name="file">
            </div>
            <button type="submit" class="btn btn-primary float-end"> Submit </button>
        </form>
        </div>
    </div>
            @if(session()->has('success'))
            {{ session()->get('success')}}
            
<?php
$nama_file=session()->get('success');
$data = file('file/'.$nama_file);
$hasil = array();
$counter = 0;
$last = 0;

/**
 * Pencarian:
 * Symbol,
 * Activity Name,
 * Sequence Number, dan
 * Dependency 
 */

for ($i = 0; $i < count($data); $i++) {

    if(strpos($data[$i], '(object Message ') !== false){
        $counter++;

        $clean_tab = trim($data[$i]," "); // menghilangkan tab dan spasi kosong
        $clean_object = strpos($clean_tab, ": "); // memotong kalimat berdasarkan titik dua
        $clean_plus = $clean_object + 2; // mendapatkan angka untuk memotong 2 huruf ": "
        $clean_semicolon = substr($clean_tab, $clean_plus); // menghilangkan character berdasarkan $clean_plus
        $clean_quotes = str_replace('"', '', $clean_semicolon);
        $activity_name = trim($clean_quotes, "\n");

        // $activity_name = str_replace('"', '', trim($data[$i],"\t     (object Message "));
        // $activity_name = substr($activity_name, 3);
        // $activity_name = trim($activity_name, "\n");

        $sequence_number = str_replace('"', '', trim($data[$i+5],"\t     sequence\n"));
        $coba=(int)$sequence_number;
        $dependency = str_replace('"', '', trim($data[$i+6],"\t     ordinal\n"));
        $coba2=(int)$dependency;
        $final = $sequence_number.'->'.$dependency;
        $hasil[$i] = array(
            "symbol" => $counter,
            "activity_name" => $activity_name,
            "sequence_number" => $coba,
            "dependency" => $coba2,
        );
        $last = $i;
        $sqc[]=$coba;
        $dpc[]=$coba2;
    }
    $cek=$counter;
}

/**
 * Pencarian dependency suatu "end"
 */
// for ($i = 0; $i < count($data); $i++) {
//     if(strpos($data[$i], 'persistence') !== false){
//         $flag = $i;
//         $i = count($data); // sengaja untuk keluar perulangan
//         $end = array();
//         // Pencarian object message setelah "persistence"
//         for ($j = $flag; $j < count($data); $j++) { 
//             if(strpos($data[$j], 'sequence ') !== false){
//                 $sequence = str_replace('"', '', trim($data[$j],"sequence \t\n",));
//                 $str = (int)$sequence;//mengambil karakter berbentuk nomer
//                 $end[] = $str;
//             }
//         }
      
//         $end[] = $counter;
//         $hasil[$last + 1] = array(
//             "symbol" => $counter + 1,
//             "activity_name" => "end",
//             "sequence_number" => $counter + 1,
//             "dependency" => $end
//         );
//     }
// }
// print_r($hasil);
$akhir = '';

foreach ($hasil as $key => $value) {
    // echo gettype($value['dependency']); 
    if (is_array($value['dependency'])) {
      foreach ($value['dependency'] as $nilai) {
        $akhir .= $nilai.',';
      }    
    }
    // echo '<br>'.$key.'<br>'.$value['dependency'];
  }

  // foreach($hasil as $datas){
  //   print ($datas["dependency"]. "->" . $datas["sequence_number"] . "\n");
  //   }
?>

<div class="card  my-5 mx-5 py-5 px-5">
<H2>Sequence Diagram Table</H2>
<table class="table" id="hasil-output">
  <thead>
    <tr>
      <th>SYMBOL</th>
      <th>ACTIVITY_NAME</th>
      <th>SEQUENCE_NUMBER</th>
      <th>DEPENDENCY</th>
      <th>END</th>
    </tr>
  </thead>
  <tbody>
      @foreach($hasil as $key => $value)
    <tr>
      <td>{{ $value['symbol']}}</td>
      <td>{{$value['activity_name']}}</td>
      <td>{{$value['sequence_number']}}</td>
      <td>@if(is_array($value['dependency']))
           {{$akhir}}
          @else
            {{$value['dependency']}}
          @endif
      </td>
      <td>
      <input name="employee" type="checkbox" value="{{$value['dependency']}}"/>
      <label for="employee"></label>
      </td>
      <!-- <td>Belum soalnya array ga bisa ke string</td> -->
    </tr>
    @endforeach
    <tr>
    </tr>
  </tbody>
</table>
<input type="button" onclick="getCheckedCheckboxesFor('employee');" value="Get End" target="_blank"/>
<a type="button" class="btn btn-success" onclick="getCheckedCheckboxesFor('employee');" target="_blank" href="sdt">end</a>
</div>
<div class="card  my-5 mx-5 py-5 px-5">
  <H2>Sequence Dependency Graph</H2>
<table class="table" id="relasi-output">
  <thead>
    <tr>
      <th>DEPENDENCY</th>
      <th>SEQUENCE_NUMBER</th>
    </tr>
  </thead>
  <tbody>
      @foreach($hasil as $key => $value)
    <tr>
      <td>@if(is_array($value['dependency']))
        {{$akhir}}
        @else
        {{$value['dependency']}}
        @endif
      </td>
      <td>{{$value['sequence_number']}}</td>
    </tr>
    @endforeach
    <tr>
    </tr>
  </tbody>
</table>
  </div>
<div class="card  my-5 mx-5 py-5 px-5">
  <H2>Graph Sistem</H2>
<table class="table" id="hashmap-output">
  <thead>
    <tr>
      <th>Key</th>
      <th>Value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    </tr>
  </tbody>
</table>
  </div>
  <div class="card  my-5 mx-5 py-5 px-5" id = "dfs"> 
  <H2>Test Path</H2>
  <th></th>
  </div>
  
@endif
<script >
  /**
 * Pengecekan end pada ceklis
 */
  function getCheckedCheckboxesFor(checkboxName) {
    const cek={!!json_encode($counter)!!}+1;
    const sqc={!!json_encode($sqc)!!};
    const dpc={!!json_encode($dpc)!!};

    var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'),values = [];
    Array.prototype.forEach.call(checkboxes, function(el) {
      values.push(el.value);
    });
    //menampilkan tabel
      document.getElementById("hasil-output").innerHTML += "<td>"+cek+"</td>   <td>End</td>   <td>"+cek+"</td>   <td>"+values+"</td";
      document.getElementById("relasi-output").innerHTML += "<td>"+values+"</td>  <td>"+cek+"</td>";
    //ngecek hashmap key yang ada endnya
      for(var i=1;i<cek-1;i++)
    {
      var end = 0;
      for(var j=0;j<values.length;j++)
      {
        if(dpc[i]==values[j])
        {
          end=1;
        }
      }
      dpc.sort(function(a, b){return a - b});
      sqc.sort(function(a, b){return a - b});
      values.sort(function(a, b){return a - b});
      if(end==1)
      {
        console.log(end);
        document.getElementById("hashmap-output").innerHTML += "<td>"+dpc[i]+"</td>  <td>"+sqc[i]+","+cek+"</td>";
      }
      else{
        document.getElementById("hashmap-output").innerHTML += "<td>"+dpc[i]+"</td>  <td>"+sqc[i]+"</td>";

      }
    }
  /**
 * Penerapan metode depth firsh search
 */
    for(var i=0;i<values.length;i++)
    {
      for(var j=0;j<sqc.length;j++)
      {
        if(sqc[j]==values[i])
        {
          console.log(j+1);
          console.log(cek);
          document.getElementById("dfs").innerHTML += (j+1)+", "+cek+"<br>";
          j=sqc.length;
        }
        else{
          console.log(j+1);
          document.getElementById("dfs").innerHTML += (j+1)+", ";
        }
      }
    }

  }

  


</script>
    </body>
</html>